package com.cooperativismo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cooperativismo.domain.dto.VotacaoDTO;
import com.cooperativismo.manager.VotacaoManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin
@Tag(name = "GERENCIAMENTO VOTAÇÃO")
@RequestMapping("votacao")
public class VotacaoController {
	
	@Autowired
	private VotacaoManager manager;

	@Parameter(description = "ID da Pauta", name = "idPauta", required = true)
	@Operation(tags = "Consultas", summary = "Consultar Votos por Pauta", description = "Lista todos os votos por pauta.", operationId = "buscarVotosPauta")
	@GetMapping("pautas/{idPauta}/votacao")
	public ResponseEntity<VotacaoDTO> findVotosByPautaId(@PathVariable Long idPauta) {
		return ResponseEntity.ok(manager.buscarVotosPauta(idPauta));
	}
}
