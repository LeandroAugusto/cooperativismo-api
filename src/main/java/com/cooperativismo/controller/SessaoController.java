package com.cooperativismo.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cooperativismo.domain.model.Sessao;
import com.cooperativismo.manager.SessaoManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin
@RequestMapping("sessao")
@Tag(name = "GERENCIAMENTO SESSAO")
public class SessaoController {

	@Autowired
	private SessaoManager manager;
	
	@GetMapping("listar")
	@Operation(tags = "Consultas", summary = "Listar Sessões", description = "Lista todas as sessões cadastradas.", operationId = "listarSessoes")
	public ResponseEntity<List<Sessao>> listarSessoes(){
		return ResponseEntity.ok(manager.listarSessoes());
	}
	
	@Parameter(description = "ID da Pauta", name = "idPauta", required = true)
	@GetMapping("pautas/{idPauta}")
	@Operation(tags = "Consultas", summary = "Buscar Sessões", description = "Buscar sessão por id da pauta.", operationId = "buscarSessoes")
	public ResponseEntity<List<Sessao>> buscarSessaoPorPauta(@PathVariable("idPauta") Long idPauta){
		return ResponseEntity.ok(manager.buscarPorSessaoPauta(idPauta));
	}
	
	@Parameter(description = "ID da Pauta", name = "idPauta", required = true)
	@PostMapping("criar/{idPauta}")
	@Operation(tags = "Cadastro", summary = "Criar Sessão", description = "Cadastrar uma nova sessão.", operationId = "criarSessao")
	public ResponseEntity<Sessao> criarSessaoPauta(@PathVariable("idPauta") Long idPauta, @Valid @RequestBody Sessao sessao){
		Sessao registroSalvo = manager.criarSessao(idPauta, sessao);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(registroSalvo.getId()).toUri();
		return ResponseEntity.created(uri).body(registroSalvo);
	}
	
	@Parameter(description = "ID da Sessão", name = "id", required = true)
	@Operation(tags = "Exclusão", summary = "Exclusão de Sessão", description = "Excluir uma sessao.", operationId = "excluirSessao")
	@DeleteMapping("deletar/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteSessao(@PathVariable Long id) {
		manager.deletarSessao(id);
	}
}
