package com.cooperativismo.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cooperativismo.domain.model.Pauta;
import com.cooperativismo.manager.PautaManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin
@RequestMapping("pauta")
@Tag(name = "GERENCIAMENTO PAUTA")
public class PautaController {
	
	@Autowired
	private PautaManager manager;
	
	@Operation(tags = "Consultas", summary = "Listar Pautas", description = "Lista todas as pautas cadastradas.", operationId = "listarPautas")
	@GetMapping("listar")
	public ResponseEntity<List<Pauta>> listar() {
		return ResponseEntity.ok(manager.listarPautas());
	}

	@Operation(tags = "Cadastro", summary = "Cadastrar Pautas", description = "Cadastrar uma nova pauta.", operationId = "salvarPauta")
	@PostMapping("salvar")
	public ResponseEntity<Pauta> salvar(@Valid @RequestBody Pauta pauta) {
		Pauta registroSalvo = manager.salvar(pauta);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(registroSalvo.getId()).toUri();
		return ResponseEntity.created(uri).body(registroSalvo);
	}
	
	@Parameter(description = "ID da Pauta", name = "id", required = true)
	@Operation(tags = "Atualização", summary = "Atualizar Pauta", description = "Atualizar dados de uma pauta.", operationId = "atualizarPauta")
	@PutMapping("atualizar/{id}")	
	public ResponseEntity<Pauta> atualizar(@PathVariable("id") Long id, @Valid @RequestBody Pauta pauta) {
		return ResponseEntity.ok(manager.atualizar(id, pauta));
	}

	@Parameter(description = "ID da Pauta", name = "id", required = true)
	@Operation(tags = "Exclusão", summary = "Exclusão de Pauta", description = "Excluir uma pauta.", operationId = "excluirPauta")
	@DeleteMapping("deletar/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		manager.excluirPauta(id);
	}
}
