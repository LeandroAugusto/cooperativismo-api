package com.cooperativismo.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cooperativismo.domain.model.Associado;
import com.cooperativismo.manager.AssociadoManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin
@RequestMapping("/associado/")
@Tag(name = "GERENCIAMENTO ASSOCIADO")
public class AssociadoController{
	
	@Autowired
	private AssociadoManager manager;
	
	@Operation(tags = "Consultas", summary = "Listar Associados", description = "Lista todos os associados cadastrados.", operationId = "listarAssociados")
	@GetMapping("listar")
	public ResponseEntity<List<Associado>> listar() {
		return ResponseEntity.ok(manager.listarAssociados());
	}

	@Operation(tags = "Cadastro", summary = "Cadastrar Associado", description = "Cadastrar um novo associado.", operationId = "salvarAssociado")
	@PostMapping("salvar")
	public ResponseEntity<Associado> salvar(@Valid @RequestBody Associado associado) {
		Associado registroSalvo = manager.salvar(associado);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(registroSalvo.getId()).toUri();
		return ResponseEntity.created(uri).body(registroSalvo);
	}
	
	@Parameter(description = "ID do Associado", name = "id", required = true)
	@Operation(tags = "Atualização", summary = "Atualizar Associado", description = "Atualizar dados de um associado.", operationId = "atualizarAssociado")
	@PutMapping("atualizar/{id}")	
	public ResponseEntity<Associado> atualizar(@PathVariable("id") Long id, @Valid @RequestBody Associado associado) {
		return ResponseEntity.ok(manager.atualizar(id, associado));
	}

	@Parameter(description = "ID do Associado", name = "id", required = true)
	@Operation(tags = "Exclusão", summary = "Exclusão de Associado", description = "Excluir um associado.", operationId = "excluirAssociado")
	@DeleteMapping("deletar/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		manager.excluirAssociado(id);
	}
}
