package com.cooperativismo.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cooperativismo.domain.model.Voto;
import com.cooperativismo.manager.VotoManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin
@RequestMapping("votos")
@Tag(name = "GERENCIAMENTO VOTO")
public class VotoController {
	
	@Autowired
	private VotoManager manager;
	
	@Operation(tags = "Consultas", summary = "Listar Votos", description = "Lista todos os votos.", operationId = "listarVotos")
	@GetMapping("listar")
	public ResponseEntity<List<Voto>> listar() {
		return ResponseEntity.ok(manager.listarVotos());
	}
	
	@Parameter(description = "ID do Voto", name = "id", required = true)
	@Operation(tags = "Consultas", summary = "Buscar Votos por ID", description = "Buscar votos por ID.", operationId = "buscarVotoPorId")
	@GetMapping("{id}")
	public ResponseEntity<Voto> findVotoById(@PathVariable Long id) {
		return ResponseEntity.ok(manager.findById(id));
	}

	@Parameter(description = "ID da Pauta", name = "idPauta", required = true)
	@Operation(tags = "Consultas", summary = "Buscar Votos por Sessão", description = "Buscar votos por sessão.", operationId = "buscarVotoPorSessao")
	@GetMapping("sessoes/{idPauta}")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<List<Voto>> buscarVotosPorPauta(@PathVariable Long idPauta) {
		return ResponseEntity.ok(manager.buscarVotosPorPauta(idPauta));
	}

	@Parameter(description = "ID da Pauta", name = "idPauta", required = true)
	@Parameter(description = "ID da Sessão", name = "idSessao", required = true)
	@Parameter(description = "ID do Associado", name = "idAssociado", required = true)
	@Operation(tags = "Cadastro", summary = "Criar Votos", description = "Criar um novo voto.", operationId = "criarVoto")
	@PostMapping("/pautas/{idPauta}/sessoes/{idSessao}/associado/{idAssociado}")
	public ResponseEntity<Voto> createVoto(@PathVariable Long idPauta, @PathVariable Long idSessao, @PathVariable("idAssociado") Long idAssociado, @RequestBody Voto voto) {
		Voto registroSalvo = manager.criarVoto(idPauta, idSessao, idAssociado, voto);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(registroSalvo.getId()).toUri();
		return ResponseEntity.created(uri).body(registroSalvo);
	}
	
	@Parameter(description = "ID do Voto", name = "id", required = true)
	@Operation(tags = "Exclusão", summary = "Exclusão de Voto", description = "Excluir um voto.", operationId = "excluirVoto")
	@DeleteMapping("deletar/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deletar(@PathVariable Long id) {
		manager.deletar(id);
	}
}
