package com.cooperativismo.domain.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "SESSAO")
public class Sessao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_SESSAO")
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "ID_PAUTA", referencedColumnName = "ID_PAUTA")
	private Pauta pauta;

	@Column(name = "DATA_INI_PAUTA")
	private LocalDateTime dataPauta;
	
	@Column(name = "VALIDADE")
	private Long validade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pauta getPauta() {
		return pauta;
	}

	public void setPauta(Pauta pauta) {
		this.pauta = pauta;
	}

	public LocalDateTime getDataPauta() {
		return dataPauta;
	}

	public void setDataPauta(LocalDateTime dataPauta) {
		this.dataPauta = dataPauta;
	}

	public Long getValidade() {
		return validade;
	}

	public void setValidade(Long validade) {
		this.validade = validade;
	}
}
