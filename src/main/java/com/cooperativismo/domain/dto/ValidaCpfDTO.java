package com.cooperativismo.domain.dto;

public class ValidaCpfDTO {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
