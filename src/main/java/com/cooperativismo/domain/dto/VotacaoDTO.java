package com.cooperativismo.domain.dto;

import com.cooperativismo.domain.model.Pauta;

public class VotacaoDTO {

	private Pauta pauta;
	private Long totalSim;
	private Long totalNao;
	private Long totalVotos;
	private Long totalSessoes;

	public Pauta getPauta() {
		return pauta;
	}

	public Long getTotalSim() {
		return totalSim;
	}

	public void setTotalSim(Long totalSim) {
		this.totalSim = totalSim;
	}

	public Long getTotalNao() {
		return totalNao;
	}

	public void setTotalNao(Long totalNao) {
		this.totalNao = totalNao;
	}

	public Long getTotalVotos() {
		return totalVotos;
	}

	public void setTotalVotos(Long totalVotos) {
		this.totalVotos = totalVotos;
	}

	public Long getTotalSessoes() {
		return totalSessoes;
	}

	public void setTotalSessoes(Long totalSessoes) {
		this.totalSessoes = totalSessoes;
	}

	public void setPauta(Pauta pauta) {
		this.pauta = pauta;
	}

}
