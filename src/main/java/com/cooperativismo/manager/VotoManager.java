package com.cooperativismo.manager;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.cooperativismo.domain.dto.ValidaCpfDTO;
import com.cooperativismo.domain.model.Associado;
import com.cooperativismo.domain.model.Sessao;
import com.cooperativismo.domain.model.Voto;
import com.cooperativismo.exception.CooperativismoAlreadyReportedException;
import com.cooperativismo.exception.CooperativismoBadRequestException;
import com.cooperativismo.exception.CooperativismoConflictException;
import com.cooperativismo.exception.CooperativismoNotFoundException;
import com.cooperativismo.exception.CooperativismoTimeOutException;
import com.cooperativismo.exception.CooperativismoValidationException;
import com.cooperativismo.manager.client.ValidaCpf;
import com.cooperativismo.repository.VotoRepository;

@Component
public class VotoManager {
	
	private static final String CPF_UNABLE = "UNABLE_TO_VOTE";
	
	@Autowired
	private VotoRepository votoRepo;

	@Autowired
	private ValidaCpf validarCpf;

	@Autowired
	private SessaoManager sessaoManager;
	
	@Autowired
	private AssociadoManager associadoManager;
		
	public Voto findById(Long id) {
		Optional<Voto> findById = votoRepo.findById(id);
		if(!findById.isPresent()){
			throw new CooperativismoConflictException("Voto não encontrado");
		}
		return findById.get();
	}
	
	public List<Voto> buscarVotosPorPauta(Long idPauta) {
		return votoRepo.findByPautaId(idPauta);
	}
	
	public List<Voto> listarVotos() {
		return votoRepo.findAll();
	}
	
	public Voto criarVoto(Long idPauta, Long idSessao, Long idAssociado, Voto voto) {
		Sessao sessao = sessaoManager.buscarPorId(idSessao);
		if (!idPauta.equals(sessao.getPauta().getId())) {
			throw new CooperativismoNotFoundException("Sessão Inválida");
		}
		
		Associado associado = associadoManager.buscarPorId(idAssociado);
		
		voto.setPauta(sessao.getPauta());
		voto.setAssociado(associado);
		
		validaCpf(voto);
		verificarVotoExistente(voto);
		verificarTempo(sessao);
		return votoRepo.save(voto);
	}

	public void deletar(Long id) {
		votoRepo.deleteById(id);
	}
	
	protected void verificarTempo(Sessao sessao) {
		LocalDateTime dataLimite = sessao.getDataPauta().plusMinutes(sessao.getValidade());
		if (LocalDateTime.now().isAfter(dataLimite)) {
			throw new CooperativismoTimeOutException("Tempo de Sessão Expirada");
		}
	}
	
	protected void verificarVotoExistente(Voto voto) {
		Optional<Voto> votoEncontrado = votoRepo.findByAssociadoCpfAndPautaId(voto.getAssociado().getCpf(), voto.getPauta().getId());

		if (votoEncontrado.isPresent()) {
			throw new CooperativismoAlreadyReportedException("Voto já existe!");
		}
	}
	
	protected void validaCpf(Voto voto) {
		ResponseEntity<ValidaCpfDTO> cpfValidado = validarCpf.validaCpf(voto.getAssociado().getCpf());
		if (HttpStatus.OK.equals(cpfValidado.getStatusCode())) {
			if (CPF_UNABLE.equalsIgnoreCase(cpfValidado.getBody().getStatus())) {
				throw new CooperativismoValidationException("CPF não autorizado");
			}
		} else {
			throw new CooperativismoBadRequestException("CPF Inválido");
		}
	}
}
