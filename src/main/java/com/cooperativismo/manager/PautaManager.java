package com.cooperativismo.manager;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cooperativismo.domain.model.Pauta;
import com.cooperativismo.exception.CooperativismoConflictException;
import com.cooperativismo.repository.PautaRepository;

@Component
public class PautaManager {
	
	@Autowired
	private PautaRepository pautaRepo;

	public List<Pauta> listarPautas() {
		return pautaRepo.findAll();
	}

	public Pauta salvar(Pauta pauta) {
		return pautaRepo.save(pauta);
	}

	public Pauta atualizar(Long id, Pauta newPauta) {
		Optional<Pauta> localizado = pautaRepo.findById(id);

		if (!localizado.isPresent()) {
			throw new CooperativismoConflictException("Não foi localizado nenhuma pauta com o id: " + id);
		}

		Pauta pauta = localizado.get();
		BeanUtils.copyProperties(newPauta, pauta, "id");
		return pautaRepo.save(pauta);
	}
	
	public void excluirPauta(Long id) {
		pautaRepo.deleteById(id);
	}
}
