package com.cooperativismo.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cooperativismo.domain.dto.VotacaoDTO;
import com.cooperativismo.domain.model.Pauta;
import com.cooperativismo.domain.model.Voto;
import com.cooperativismo.exception.CooperativismoNotFoundException;
import com.cooperativismo.repository.SessaoRepository;
import com.cooperativismo.repository.VotoRepository;

@Component
public class VotacaoManager {
	
	@Autowired
	private VotoRepository votoRepository;
	
	@Autowired
	private SessaoRepository sessaoRepository;
	
	public VotacaoDTO buscarVotosPauta(Long idPauta) {
		VotacaoDTO dto = new VotacaoDTO();
		
		List<Voto> listaVotos = votoRepository.findByPautaId(idPauta);
		
		if (listaVotos.isEmpty()) {
			throw new CooperativismoNotFoundException("Nenhum voto encontrado pela pauta: " + idPauta);
		}
		
		Pauta pauta = listaVotos.iterator().next().getPauta();
		Long totalVotos = Long.valueOf(listaVotos.size());
		Long totalSessoes = Long.valueOf(sessaoRepository.countByPautaId(pauta.getId()));
		Long totalSim = listaVotos.stream().filter(x -> Boolean.TRUE.equals(x.getEscolha())).count();
		Long totalNao = (totalVotos - totalSim);
		
		dto.setPauta(pauta);
		dto.setTotalVotos(totalVotos);
		dto.setTotalSessoes(totalSessoes);
		dto.setTotalSim(totalSim);
		dto.setTotalNao(totalNao);
		
		return dto;
	}
}
