package com.cooperativismo.manager;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cooperativismo.domain.model.Associado;
import com.cooperativismo.exception.CooperativismoConflictException;
import com.cooperativismo.repository.AssociadoRepository;

@Component
public class AssociadoManager {

	@Autowired
	private AssociadoRepository associadoRepo;

	public List<Associado> listarAssociados() {
		return associadoRepo.findAll();
	}
	
	public Associado buscarPorId(Long id) {
		Optional<Associado> associado = associadoRepo.findById(id);
		
		if(!associado.isPresent()) {
			throw new CooperativismoConflictException("Não foi localizado nenhum associado com o id: " + id);
		}
		
		return associado.get();
	}

	public Associado salvar(Associado associado) {
		return associadoRepo.save(associado);
	}

	public Associado atualizar(Long id, Associado newAssociado) {
		Optional<Associado> localizado = associadoRepo.findById(id);

		if (!localizado.isPresent()) {
			throw new CooperativismoConflictException("Não foi localizado nenhum associado com o id: " + id);
		} 
		
		Associado associado = localizado.get();
		BeanUtils.copyProperties(newAssociado, associado, "id");
		return associadoRepo.save(associado);
	}
	
	public void excluirAssociado(Long id) {
		associadoRepo.deleteById(id);
	}
}
