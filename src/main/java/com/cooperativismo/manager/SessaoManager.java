package com.cooperativismo.manager;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cooperativismo.domain.model.Pauta;
import com.cooperativismo.domain.model.Sessao;
import com.cooperativismo.exception.CooperativismoConflictException;
import com.cooperativismo.repository.PautaRepository;
import com.cooperativismo.repository.SessaoRepository;

@Component
public class SessaoManager {

	@Autowired
	private SessaoRepository sessaoRepo;

	@Autowired
	private PautaRepository pautaRepo;

	public List<Sessao> listarSessoes() {
		return sessaoRepo.findAll();
	}
	
    public Sessao buscarPorId(Long id) {
        Optional<Sessao> registroLocalizado = sessaoRepo.findById(id);
        if(!registroLocalizado.isPresent()){
            throw new CooperativismoConflictException("Não foi localizado nenhuma sessão com o id: " + id);
        }
        return registroLocalizado.get();
    }

    public List<Sessao> buscarPorSessaoPauta(Long idPauta) {
    	Optional<List<Sessao>> listaEncontrada = sessaoRepo.findByPautaId(idPauta);
    	
    	if(!listaEncontrada.isPresent()) {
    		throw new CooperativismoConflictException("Não foi localizado nenhuma sessão pelo id da pauta: " + idPauta);
    	}
    	
        return listaEncontrada.get();
    }
	
    public Sessao criarSessao(Long idPauta, Sessao sessao){
    	
    	if(sessao.getDataPauta() == null) {
    		sessao.setDataPauta(LocalDateTime.now());
    	}
    	
    	if(sessao.getValidade() == null) {
    		sessao.setValidade(1L);
    	}
    	
        Optional<Pauta> pautaLocalizada = pautaRepo.findById(idPauta);
        if(!pautaLocalizada.isPresent()){
            throw new CooperativismoConflictException("Não foi localizado nenhuma pauta com o id: " + idPauta);
        }
        sessao.setPauta(pautaLocalizada.get());
        return sessaoRepo.save(sessao);
    }
    
    public void deletarSessao(Long id) {
        sessaoRepo.deleteById(id);
    }
}
