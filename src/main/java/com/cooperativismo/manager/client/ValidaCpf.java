package com.cooperativismo.manager.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.cooperativismo.domain.dto.ValidaCpfDTO;

@FeignClient(value = "validacao", url = "${url-valida-cpf}")
public interface ValidaCpf {
	
	@GetMapping("/{cpf}") 
	ResponseEntity<ValidaCpfDTO> validaCpf(@PathVariable("cpf") String cpf);
}
