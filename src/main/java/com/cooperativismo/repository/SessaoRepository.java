package com.cooperativismo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cooperativismo.domain.model.Sessao;

public interface SessaoRepository extends JpaRepository<Sessao, Long>{
	
    Integer countByPautaId(Long id);

    Optional<List<Sessao>> findByPautaId(Long id);
}
