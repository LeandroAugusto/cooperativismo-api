package com.cooperativismo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cooperativismo.domain.model.Pauta;

public interface PautaRepository extends JpaRepository<Pauta, Long>{

}
