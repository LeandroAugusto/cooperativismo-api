package com.cooperativismo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cooperativismo.domain.model.Associado;

@Repository
public interface AssociadoRepository extends JpaRepository<Associado, Long>{
	
}
