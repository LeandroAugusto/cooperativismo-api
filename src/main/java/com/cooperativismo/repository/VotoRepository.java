package com.cooperativismo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cooperativismo.domain.model.Voto;

public interface VotoRepository extends JpaRepository<Voto, Long>{
	
    Optional<Voto> findByAssociadoCpf(String cpf);

    List<Voto> findByPautaId(Long id);

    Optional<Voto> findByAssociadoCpfAndPautaId(String cpf, Long idPauta);
}
