package com.cooperativismo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
public class CooperativismoValidationException extends RuntimeException{
	
	private static final long serialVersionUID = 5149063910593546661L;

	public CooperativismoValidationException(String message) {
		super(message);
	}
}
