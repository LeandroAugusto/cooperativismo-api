package com.cooperativismo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class CooperativismoNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 6354873578818841227L;

	public CooperativismoNotFoundException(String message) {
		super(message);
	}
}
