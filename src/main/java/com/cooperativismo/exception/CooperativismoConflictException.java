package com.cooperativismo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class CooperativismoConflictException extends RuntimeException{
	
	private static final long serialVersionUID = -1304081848634371217L;

	public CooperativismoConflictException(String message) {
		super(message);
	}
	
	
}
