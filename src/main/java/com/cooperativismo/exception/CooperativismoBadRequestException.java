package com.cooperativismo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class CooperativismoBadRequestException extends RuntimeException{

	private static final long serialVersionUID = 4678949243803453715L;

	public CooperativismoBadRequestException(String message) {
		super(message);
	}
	
	
}
