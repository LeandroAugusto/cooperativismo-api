package com.cooperativismo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.ALREADY_REPORTED)
public class CooperativismoAlreadyReportedException extends RuntimeException{
	
	private static final long serialVersionUID = -2474185863041917879L;

	public CooperativismoAlreadyReportedException(String message) {
		super(message);
	}
}
