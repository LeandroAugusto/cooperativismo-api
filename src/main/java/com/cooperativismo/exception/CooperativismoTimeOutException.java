package com.cooperativismo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.REQUEST_TIMEOUT)
public class CooperativismoTimeOutException extends RuntimeException{
	
	private static final long serialVersionUID = -2474185863041917879L;

	public CooperativismoTimeOutException(String message) {
		super(message);
	}
}
