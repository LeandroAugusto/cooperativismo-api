package com.cooperativismo.testes;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.cooperativismo.CooperativismoApplication;
import com.cooperativismo.manager.VotacaoManager;
import com.cooperativismo.manager.VotoManager;
import com.cooperativismo.manager.client.ValidaCpf;
import com.cooperativismo.repository.VotoRepository;

@SpringBootTest(classes = CooperativismoApplication.class)
@EnableJpaRepositories
public class VotoManagerTest {

	@Autowired
	private ValidaCpf validaCpf;

	@Autowired
	private VotoManager votoManager;

	@Autowired
	private VotoRepository votoRepository;
	
	@Autowired
	private VotacaoManager votacaoManager;
	
	@Test
	void buscarVotoIdTest() {
		String cpfQueDeveRetornar = "01271807297";
		
		assertEquals(cpfQueDeveRetornar,
				votoManager.findById(278L).getAssociado().getCpf());
	}
	
	@Test
	void buscarVotoPautaTest() {
		String cpfQueDeveRetornar = "01271807297";
		
		assertEquals(cpfQueDeveRetornar,
				votoManager.buscarVotosPorPauta(258L).get(0).getAssociado().getCpf());
	}
	
	@Test
	void validaCpfTest() {
		String valorQueDeveRetornar = "ABLE_TO_VOTE";
		
		assertEquals(valorQueDeveRetornar,
				validaCpf.validaCpf("01271807297").getBody().getStatus());
	}
	
	@Test
	void validaCpfTest2() {
		String valorQueDeveRetornar = "UNABLE_TO_VOTE";
		
		assertEquals(valorQueDeveRetornar,
				validaCpf.validaCpf("01271808269").getBody().getStatus());
	}
	
	@Test
	void verificarVotoJaExisteTest() {
		Boolean valorQueDeveRetornar = true;
		
		assertEquals(valorQueDeveRetornar,
				votoRepository.findByAssociadoCpfAndPautaId("01271807297", 258L).get().getEscolha());
	}
	
	@Test
	void verificarQtdVotosSimTest() {
		Long qtdEsperada = 1L;
		
		assertEquals(qtdEsperada,
				votacaoManager.buscarVotosPauta(258L).getTotalSim());
	}
	
	@Test
	void verificarQtdVotosNaoTest() {
		Long qtdEsperada = 0L;
		
		assertEquals(qtdEsperada,
				votacaoManager.buscarVotosPauta(258L).getTotalNao());
	}
	
	@Test
	void verificarQtdSessoesTest() {
		Long qtdEsperada = 14L;
		
		assertEquals(qtdEsperada,
				votacaoManager.buscarVotosPauta(258L).getTotalSessoes());
	}
	
	@Test
	void verificarQtdVotosTest() {
		Long qtdEsperada = 1L;
		
		assertEquals(qtdEsperada,
				votacaoManager.buscarVotosPauta(258L).getTotalVotos());
	}
}
